#include <stdio.h>
#include <stdlib.h>

void insertion_sort(int * v, long int n) {
    long int i, j, x;
    for(i=+1; i<=n; i++){
        x = v[i];
        j = i;
        while(x < v[j-1] && j > 0){
            v[j] = v[j-1];
            j--;
        }
        v[j] = x;
    }
}

void merge_sort(int i, int j, int a[], int aux[]) {
    //if array vazio ou com 1 elemento
    if (j <= i) {
    }

    int meio = (i + j) / 2;

    // if array ordenado -> não efetua o merge sort
    if(a[meio] <= a[meio + 1]) {
        return;
    }

    // if arrayaté 10 elementos -> faz insertion sort no lugar de merge sort
    if ((j+i) <= 10) {
        insertion_sort(a, j);
        return;
    }

    merge_sort(i, meio, a, aux);
    merge_sort(meio + 1, j, a, aux);

    int esq = i;
    int dir = meio + 1;
    int k;

    for (k = i; k <= j; k++) {
        if (esq == meio + 1) {
            aux[k] = a[dir];
            dir++;
        } else if (dir == j + 1) {
            aux[k] = a[esq];
            esq++;
        } else if (a[esq] < a[dir]) {
            aux[k] = a[esq];
            esq++;
        } else {
            aux[k] = a[dir];
            dir++;
        }
    }

    for (k = i; k <= j; k++) {
        a[k] = aux[k];
    }
}

int main() {
    int *a, n, i;

    printf("Tamanho do array: \n");
    scanf("%d", &n);
    a = (int *) malloc(sizeof(int) * n);

    printf("Elemento: \n", n);

    for (i = 0; i < n; i++)
        scanf("%d", &a[i]);

    merge_sort(0, n - 1, a, a);

    printf("Array ordenado: \n");

    for (i = 0; i < n; i++)
        printf("%d\n", a[i]);

    return 0;
}
