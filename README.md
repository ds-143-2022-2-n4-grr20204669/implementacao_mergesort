# Otimização Mergesort

Implemente o Mergesort recursivo OU iterativo com as otimizações abordadas na video-aula
* Utilizar Insertsort em partes pequenas do array;
* Não realizar a operação merge caso a coleção já esteja ordenada;
* Eliminar a cópia para um array auxiliar;
* Aproveitar trechos pré-ordenados;

-> Coloque a implementação em um repositório do gitlab e envie o link como resposta nessa tarefa.
